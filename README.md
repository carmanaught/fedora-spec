# Fedora Spec

This repository is for my own Fedora Spec files, with personal changes and patches. This was used for a Copr, but is now for use in doing local package builds, so this is an online copy.

These spec files are for personal package changes and should not be depended upon for specific versions of packages as I may or may not update as I see fit, for whichever version of Fedora I may be using.
